<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhotoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('photos', function(Blueprint $table){
        $table->increments('id');
        $table->string('url');
        $table->timestamps();
        $table->integer('album_id')->unsigned();
        $table->foreign('album_id')
          ->references('id')
          ->on('albums')
          ->onDelete('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('photos',function(Blueprint $table){
          $table->dropForeign('photos_album_id_foreign');
        });
    }
}
