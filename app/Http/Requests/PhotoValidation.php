<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PhotoValidation extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo.url' => [
              'required',
              'min:5',
              'regex:#(https|http)://.*(jpg|png|gif)$#i'
            ]
        ];
    }
}
