<?php

namespace App\Http\Middleware;

use Closure;

class AddHeaders
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('Set-Cookie', 'XSRF-TOKEN='.csrf_token());
        return $response;
    }
}