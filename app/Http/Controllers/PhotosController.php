<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Photo;
use App\Album;
use App\Http\Requests\PhotoValidation;

class PhotosController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoValidation $request)
    {
      $album = Album::find($request->photo['album_id']);
      $photo = new Photo($request->photo);
      $album->photos()->save($photo);
      return response()->json($photo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Photo::destroy($id);
    }
}
